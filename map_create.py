"""
This script will create numpy array data data according to bd map image
"""
import matplotlib.pyplot as plt
img = plt.imread("map/bd_map.jpg")
# district code
colors = [
    '[255 255 255]', #blank -1
    '[233 198 178]', #ran 0
    '[244 198 226]', #raj 1
    '[216 239 245]', #khu 2
    '[255 230 129]', #mim 3
    '[245 237 216]', #syl 4
    '[255 169 168]', #dha 5
    '[247 255 214]', #bar 6
    '[222 233 175]'] #chi 7

import numpy as np
file = open('np_data.data','wb')
# map size is 1500X1800 px
data_array = np.zeros( (1800, 1500) )
for i in range(1800):
    for j in range(1500):
        if str(img[i][j]) in colors:
            data_array[i][j] = colors.index(str(img[i][j]))-1
        else: data_array[i][j] = -2
    print(i)
np.save(file,data_array)
# print Color from map
# print(img[270,340])  #ran
# print(img[660, 360])  #raj
# print(img[1060, 440])  #khu
# print(img[550, 750])  #mym
# print(img[575, 1120])  # syl
# print(img[850, 690])  # dha
# print(img[1255, 730])  # bar
# print(img[1075, 920])  # chi
fig, ax = plt.subplots()
ax.imshow(img)
plt.show()