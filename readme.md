# Corona Pandemic Simulation

## Requirements
* matplotlib
* numpy
* ffmpeg (optional)
```bash
pip install matplotlib numpy
```

## How to use it
* Generate map matrix from map use map_create.py file (Already generated)
* To run simulation use map_load.py
* To get frame vs sick patient plot use plot_final.py
* Generate video first run rename.py file then
```bash
ffmpeg -i %3d.png -vf palettegen palette.png
ffmpeg -framerate 6  -i %3d.png -c:v libx264 -strict -2 -preset slow -pix_fmt yuv420p -vf "scale=trunc(iw/2)*2:trunc(ih/2)*2" -f mp4 output.mp4
```