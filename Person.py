from random import random
from math import pi, sin, cos


class Person:
    r = 5
    theta = 2 * pi * random()
    color = 'b'
    rec_day = 0
    dist = 0

    def __init__(self, x, y, move_rate=0.5, morality=0.5, effected_rate=1.0):
        self.x = x
        self.y = y
        self.move_rate = move_rate
        self.morality = morality
        self.effected_rate = effected_rate

    def move(self, block_list=[]):
        if self.move_rate > random():
            if random() > 0.8:
                self.theta += (2 * pi * random())
            del_x = int(self.r * cos(self.theta))
            del_y = int(self.r * sin(self.theta))
            if self.theta > 2 * pi:
                self.theta -= 2 * pi
            self.x += del_x
            self.y += del_y
            ex_dis = self.dist
            from map_load import data_array
            if (1500 > self.x >= 0) and (1800 > self.y >= 0):
                if data_array[self.y][self.x] >= 0:
                    self.dist = int(data_array[self.y][self.x])
                if data_array[self.y][self.x] == -1:
                    self.x -= del_x
                    self.y -= del_y
                    self.theta += pi
                elif ex_dis != self.dist:
                    if (ex_dis in block_list or self.dist in block_list) and self.morality > random():
                        self.x -= del_x
                        self.y -= del_y
                        self.theta += pi
                        self.dist = ex_dis
            else:
                self.x -= del_x
                self.y -= del_y
                self.theta += pi
            return del_x, del_y
        return 0, 0
