"""
This script will rename all image for ffmpeg conversion
"""
import os
import glob
import shutil
root = 'img/'
dest = 'img_100000/'
if not os.path.exists(dest):
    os.makedirs(dest)
# print(glob.glob("img/*.png"))
p = sorted(glob.glob(root+'*.png'),key=os.path.getmtime)
num = 1
print(p)
for i in p:
    shutil.copyfile(i,dest+"{0:0=3d}.png".format(num))
    num+=1
    print(num)
# os.rename(r'file path\OLD file name.file type',r'file path\NEW file name.file type')
if __name__ == '__main__':
    pass