"""
This is the main script for simulation
"""
import matplotlib.pyplot as plt
from random import random
import numpy as np
from Person import Person

# per px is 366 meter
px = 366  # in meter

# load all image
img = plt.imread("map/bd_map.jpg")
chi_blk = plt.imread("map/bd_map_chi_blk.png")
khu_blk = plt.imread("map/bd_map_khu_blk.png")
bar_blk = plt.imread("map/bd_map_bar_blk.png")
dha_blk = plt.imread("map/bd_map_dha_blk.png")
syl_blk = plt.imread("map/bd_map_syl_blk.png")
mym_blk = plt.imread("map/bd_map_mym_blk.png")
ran_blk = plt.imread("map/bd_map_ran_blk.png")
raj_blk = plt.imread("map/bd_map_raj_blk.png")


dist_list = [ran_blk, raj_blk, khu_blk, mym_blk,
             syl_blk, dha_blk, bar_blk, chi_blk]

colors = [
    '#E9C6B2',  # ran 0
    '#F4C6E2',  # raj 1
    '#D8EFF5',  # khu 2
    '#FFE681',  # mim 3
    '#F5EDD8',  # syl 4
    '#FFA9A8',  # dha 5
    '#F7FFD6',  # bar 6
    '#DEE9AF']  # chi


colors_count = ['r', 'b', 'g', 'k']
labs = ["No controll", "Only mask", "mask + immobility",
        "mask + immobility + lockdown"]

# color coding
safe = 'b'
infected = 'r'
recovered = 'g'

# load numpy array data from file
file = open('np_data.data', 'rb')
data_array = np.load(file)

fig, (ax, ax2) = plt.subplots(1, 2)

persons = []
block_list = []

sick_n = 0
rec_n = 0
rec_day_count = 20 # after 20 frame sick will be recovered

init_population = []


def gen_population_div(n, dist):
    """
    :param n: Number of person in district
    :param dist: district code
    :return: list of random Persons length of n
    """
    pop = []
    for _ in range(int(n)):
        p = Person(rnd_int(0, 1500 - 1), rnd_int(0, 1800 - 1))
        while data_array[p.y][p.x] != dist:
            p = Person(rnd_int(0, 1500 - 1), rnd_int(0, 1800 - 1))
        p.dist = int(data_array[p.y][p.x])
        pop.append(p)
    return pop


"""
Sylhet Division	0.06761464322
Rangpur Division	0.1088491511
Rajshahi Division	0.127451279
Mymensingh Division	0.07839079162
Khulna Division	0.1081597051
Dhaka Division	0.2511918468
Chittagong Division	0.2009410397
Barisal Division	0.05740154341


#ran 0 0.1088491511
#raj 1 0.127451279
#khu 2 0.1081597051
#mim 3 0.07839079162
#syl 4 0.06761464322
#dha 5 0.2511918468
#bar 6 0.05740154341
#chi 7 0.2009410397
"""

# percentage of population of each district. ordered.
rate_of_population = [
    0.1088491511,
    0.127451279,
    0.1081597051,
    0.07839079162,
    0.06761464322,
    0.2511918468,
    0.05740154341,
    0.2009410397
]


def gen_population(N):
    """
    Generate N number of population
    :param N: Total number of population
    """
    global init_population
    init_population = []
    for i in range(8):
        init_population+=gen_population_div(N*rate_of_population[i],i)

    init_population+=gen_population_div(N-len(init_population),5) # fill up the fractional remaining

    # 5% of population has large mobility
    for _ in range(int(N * .005)):
        init_population[int(random() * N)].r = 100


def copy_population(N, mov, mor, eff):
    """
    This will copy init data each time to keep consistancy
    :param N: Number of population
    :param mov: mobility
    :param mor: morality (obey lockdown order)
    :param eff: chance of get infected
    :return: list of population
    """
    global persons
    population = []
    for p in init_population:
        p.move_rate = mov
        p.morality = mor
        p.effected_rate = eff
        p.color = 'b'
        population.append(p)
    rand_id = int(random() * N)
    # set first infected person in dhaka
    population[rand_id].x = 700
    population[rand_id].y = 900
    population[rand_id].color = 'r'
    population[rand_id].rec_day = rec_day_count
    return population


def rnd_int(a, b):
    """
    Generate random integer from a to b
    :param a: int from
    :param b: int to
    :return: random int
    """
    return int(a + random() * (b - a))


def check(N, lock):
    """
    check new infected person
    :param N: Number of persons
    :param lock: min number of sick person to get lockdown
    """
    global sick_n, rec_n, block_list
    ill_count = [0, 0, 0, 0, 0, 0, 0, 0]
    dr = 5 #circle radius to get infected
    for i in range(N):
        for j in range(i + 1, N):
            if persons[i].dist == persons[j].dist:
                if persons[i].color == 'r' and persons[j].color == 'b':
                    dx = abs(persons[i].x - persons[j].x)
                    dy = abs(persons[i].y - persons[j].y)
                    if (dx * dx + dy * dy) < dr * dr and persons[j].effected_rate > random():
                        persons[j].color = 'r'
                        sick_n += 1
                        persons[j].rec_day = rec_day_count
                elif persons[i].color == 'b' and persons[j].color == 'r':
                    dx = abs(persons[i].x - persons[j].x)
                    dy = abs(persons[i].y - persons[j].y)
                    if (dx * dx + dy * dy) < dr * dr and persons[i].effected_rate > random():
                        persons[i].color = 'r'
                        sick_n += 1
                        persons[i].rec_day = rec_day_count

                # elif persons[i].color == 'r' and persons[j].color == 'r':
                #     dx = abs(persons[i].x - persons[j].x)
                #     dy = abs(persons[i].y - persons[j].y)
                #     if (dx * dx + dy * dy) < dr * dr:
                #         persons[i].rec_day = rec_day_count
                #         persons[j].rec_day = rec_day_count

        if persons[i].color == 'r':
            ill_count[persons[i].dist] += 1
            persons[i].rec_day -= 1
            if persons[i].rec_day < 0:
                persons[i].color = 'g'
                rec_n += 1
                sick_n -= 1
    block_list = []
    # lockdown
    for i in range(8):
        if ill_count[i] > lock:
            block_list.append(i)
            ax.imshow(dist_list[i])
    # print(block_list)


list_of_sick_person = []
count = 1


def simulate(N, mov, mor, eff, lock):
    """
    :param N: Number of population
    :param mov: mobility
    :param mor: morality (obey lockdown order)
    :param eff: chance of get infected
    :param lock: min number of sick person to get lockdown
    """
    global sick_n, list_of_sick_person, persons, count, ax, ax2
    sick_n = 1
    persons = copy_population(N, mov, mor, eff)
    list_of_sick_person.append([])

    while sick_n > 0:
        ax.imshow(img)
        green_x = []
        green_y = []
        red_x = []
        red_y = []
        blue_x = []
        blue_y = []
        for p in persons:
            dx, dy = p.move(block_list)
            if p.color == 'b':
                blue_x.append(p.x)
                blue_y.append(p.y)
            elif p.color == 'g':
                green_x.append(p.x)
                green_y.append(p.y)
            elif p.color == 'r':
                red_x.append(p.x)
                red_y.append(p.y)
        ax.legend(bbox_to_anchor=(0.0, 1.0), loc='lower left')
        ax.scatter(blue_x, blue_y, c='b', s=2, label='Safe ({})'.format(len(blue_x)))
        ax.scatter(red_x, red_y, c='r', s=2, label='Infected ({})'.format(len(red_x)))
        ax.scatter(green_x, green_y, c='g', s=2, label='Recovered ({})'.format(len(green_x)))
        ax.legend()
        check(N, lock)

        ax2.clear()
        ax2.set_ylim(-10, N * 0.7)
        ax2.set_xlim(-10, 500)
        list_of_sick_person[-1].append(sick_n)
        final_data = open("final_data.txt", "w")
        for i in range(len(list_of_sick_person)):
            ax2.plot(list_of_sick_person[i], c=colors_count[i], label=labs[i])
            final_data.write(str(list_of_sick_person[i]))
            final_data.write('\n')
        final_data.close()
        ax2.legend()
        plt.pause(0.001)
        plt.savefig("img/" + str(count) + ".png")
        count += 1
        ax.clear()


if __name__ == '__main__':
    N = 18000
    # mov = 1
    # mor = 1
    # eff = 1
    # lock = 100
    gen_population(N)
    plt.pause(5)
    simulate(N, 1, 1, 1, N) # No controll
    simulate(N, 1, 1, 0.4, N) # Only mask
    simulate(N, 0.8, 1, 0.4, N) #mask + immobility
    simulate(N, 0.5, 1, 0.4, 100) # mask + immobility + lockdown
